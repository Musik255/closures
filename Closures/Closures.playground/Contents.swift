import UIKit



//Написать сортировку массива с помощью замыкания, в одну сторону, затем в обратную
let someIntArray = [1, 2, 8, 7, 4, 2, 9, 10, 25, -8]


//next simple bubble sort, you can use someIntArray.sorted() or someIntArray.sorted(by: >)
//or write another sort

func bubbleSortIntArray (_ array : [Int], _ f : (Int, Int) -> Bool) -> [Int]{
    assert(!array.isEmpty, "You fricked up, try another array, this array is empty")
    
    if array.count == 1{
        return array
    }
    
    var sortedArray : [Int] = array
    for i in 0..<sortedArray.count{
        for j in i + 1 ..< sortedArray.count{
            if f(sortedArray[i], sortedArray[j]){
                sortedArray.swapAt(i, j)
            }
        }
    }
    return sortedArray
}

var sortedArray = bubbleSortIntArray(someIntArray, <)

print(sortedArray)






//Создать метод, который принимает имена друзей, после этого имена положить в массив

func separateNames(_ str : String) -> [String]{ //not method
    return str.components(separatedBy: " ")
}

let strNames = "Benjamin Jhon Bob Charley Jena Paul Oliver Charley"

var arrayNames = separateNames(strNames)

//Массив отсортировать по количеству букв в имени
func sortNames(_ array : [String], _ f : (Int, Int) -> Bool) -> [String]{
    assert(!array.isEmpty, "You fricked up, try another array, this array is empty")
    
    if array.count == 1{
        return array
    }
    
    var sortedArray : [String] = array
    for i in 0..<sortedArray.count{
        for j in i + 1 ..< sortedArray.count{
            if f(sortedArray[i].count, sortedArray[j].count){
                sortedArray.swapAt(i, j)
            }
        }
    }
    return sortedArray
}

var sortedNames = sortNames(arrayNames, >)
print(sortedNames)


//Создать словарь (Dictionary), где ключ - кол-во символов в имни, а в значении - имя друга
func makeDictNames(_ arrayNames : [String]) -> [Int : [String]]{
    var dict : [Int : [String]] = [:]
    for name in arrayNames{
        if dict[name.count] == nil{
            dict[name.count] = [name]
        }
        else{
            dict[name.count]!.append(name)
        }
    }
    return dict
}

var testDict = makeDictNames(arrayNames)
print(testDict)


//Написать функцию, которая будет принимать ключ, выводить полученный ключ и значение
func printDict(_ selectedKey : Int, _ dict : Dictionary<Int, [String]>){
    
    if dict[selectedKey] == nil{
        print("key doesnt exist")
    }
    else{
        print(selectedKey, terminator: " ")
        for counter in 0..<dict[selectedKey]!.count{
            print(dict[selectedKey]![counter], terminator: " ")
        }
        print()
    }
}


printDict(4, testDict)



//Написать функцию, которая принимает 2 массива (один строковый, второй - числовой) и проверяет их на пустоту: если пустой - то добавьте любое значения и выведите массив в консоль


//Что за задание ваще?
func twoArrays(_ firstArray : inout [String], _ secondArray : inout [Int]){
    if firstArray.isEmpty{
        firstArray.append("random element")
        print(firstArray)
    }
    if secondArray.isEmpty{
        secondArray.append(Int.random(in: 0...12))
        print(secondArray)
    }
}
var inoutString : [String] = []
var inoutInt : [Int] = []

twoArrays(&inoutString, &inoutInt)

print()
print()
print()
print()
print()









//это уже не к заданию, просто разбор примеров из книжки(смотри дату предпоследнего коммита)

let wallet: [Int] = [100, 50, 5000, 10, 200, 1000, 500, 2000, 50, 500, 10, 200, 100, 5000, 1000, 200, 50, 2000, 10, 100]


func handle100(_ wallet : [Int]) -> [Int]{
    var returnWallet : [Int] = []
    for banknote in wallet{
        if banknote == 100{
            returnWallet.append(banknote)
        }
    }
    return returnWallet
}

func handleMore1000 (_ wallet : [Int]) -> [Int]{
    var returnWallet : [Int] = []
    for banknote in wallet{
        if banknote >= 1000{
            returnWallet.append(banknote)
        }
    }
    return returnWallet
}

var newWallet = handle100(wallet)
print(newWallet)






func handle(_ wallet : [Int], foo : (Int) -> Bool ) -> [Int] {
    var returnWallet : [Int] = []
    var kek = 100
    for banknote in wallet{
        if foo(banknote){
            returnWallet.append(banknote)
        }
    }
    return returnWallet
}
func compare100(_ banknote : Int) -> Bool{
    return banknote == 100
}

func compareMore1000(_ banknote : Int) -> Bool{
    return banknote >= 1000
}

newWallet = handle(wallet, foo: compareMore1000)
print(newWallet)


newWallet = handle(wallet, foo: { (banknote : Int) -> Bool in
    return banknote == 100
})
print(newWallet)


newWallet = handle(wallet, foo: {banknote in banknote >= 1000})
print(newWallet)

newWallet = handle(wallet, foo: {banknote in banknote >= 1000})
print(newWallet)
